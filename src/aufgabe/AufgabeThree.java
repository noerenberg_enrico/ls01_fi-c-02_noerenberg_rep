package aufgabe;

import java.util.HashMap;
import java.util.Map;
//Enrico Nörenberg


public class AufgabeThree {

    public static void main(String[] args) {
        System.out.printf("Fahrenheit");
        System.out.printf("%3s", "|");
        System.out.printf("%10s", "Celsius \n");
        for(int i = 0; i < 30; i++) {
            System.out.print("-");
        }

        readTable();

    }

    private static void readTable() {
        Map<Integer, Double> grad = new HashMap<>();
        grad.put(-20, -28.89);
        grad.put(-10, -23.33);
        grad.put(0, -17.78);
        grad.put(20, -6.67);
        grad.put(30, -1.11);

        for(Integer s : grad.keySet()) {
            System.out.printf("\n" + s);
            if(s < 9 && s != 0) {
                System.out.printf("%10s", "|");
            } else if(s < 0){
                System.out.printf("%10s", "|");
            } else if(s == 0) {
                System.out.printf("%12s", "|");
            } else {
                System.out.printf("%11s", "|");
            }
            System.out.printf("%10.2f", grad.get(s));
        }
    }
}

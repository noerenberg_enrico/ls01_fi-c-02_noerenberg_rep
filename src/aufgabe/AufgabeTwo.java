package aufgabe;

public class AufgabeTwo {

    public static void main(String[] args) {
        for(int i = 0; i < 6; i++) {
            showFakultaet(i);
        }
    }

    private static void showFakultaet(int zahl) {
        System.out.printf( "%-5s", "!"+zahl);
        System.out.printf("=");
        for(int j = 1; j <= zahl; j++) { //j = 5
            System.out.printf( "%2s", j);
            if(j < zahl) { //j = 5, i = 3
                System.out.printf("%2s", "*");
            }
        }
        int end = 1;
        for(int j = 1; j <= zahl; j++) {
            end = j * end;
        }
        System.out.printf("%3s","=");
        System.out.printf("%4s", String.valueOf(end));
        System.out.printf( "%5s", "\n");
    }
}

package aufgabe;
import java.util.InputMismatchException;
import java.util.Scanner;
public class Fahrkartenautomat
{

    private static void fahrkartenbestellungErfassen(Scanner tastatur) {


        if(zuZahlenderBetrag == 0.0) {
            System.out.print("Zu zahlender Betrag (EURO): ");
            zuZahlenderBetrag = tastatur.nextDouble();
        }

        System.out.print("Anzahl der Fahrkarten: ");//Aufgabe 1
        fahrkartenAnzahl = tastatur.nextInt(); //Aufgabe 1
        if(fahrkartenAnzahl >= 10 || fahrkartenAnzahl == 0) {
            System.out.println("Bitte geben Sie eine angemaesse Anzahl an.");
            System.out.println("Es sind maximal 10 Tickets erlaubt");
            fahrkartenbestellungErfassen(tastatur);
        } else {
            zuZahlenderBetrag = zuZahlenderBetrag*fahrkartenAnzahl;
        }
        // Geldeinwurf
        // -----------
    }

    public static void fahrkartenBezahlen(Scanner tastatur) {
        while(eingezahlterGesamtbetrag < (zuZahlenderBetrag))
        {
            System.out.printf("%s %.2f %n", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag)); //Aufgabe 2
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            try {
                eingeworfeneMünze = tastatur.nextDouble();
                if(eingeworfeneMünze <= 2) {
                    eingezahlterGesamtbetrag += eingeworfeneMünze;
                } else {
                    System.out.println("Maximal 2 Euro sind erlaubt.");
                }
            } catch(InputMismatchException ex) {
                System.out.println("Sie muessen den Betrag in folgendem Format eingeben 1,1 (1 Euro 10 Cent)");
                tastatur.next();
            }
        }
    }

    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }


    public static void muenzeAusgeben(double betrag, String einheit) {
        rückgabebetrag -= betrag;
        System.out.println(betrag + einheit);
    }
    public static void rueckgeldAusgabe() {
        // Rückgeldberechnung und -Ausgabe
        // -------------------------------
        rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        rückgabebetrag = Math.round(rückgabebetrag * 100.0) / 100.0;
        System.out.println(rückgabebetrag);
        System.out.printf("%s %.2f %s %n", "Der Rückgabebetrag in Höhe von " , rückgabebetrag , " EURO");//Aufgabe 2
        System.out.println("wird in folgenden Münzen ausgezahlt:");
        while(rückgabebetrag > 0.0) {
            if(rückgabebetrag >= 2.0) {
                muenzeAusgeben(2, "Euro");
            }
            else if(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                muenzeAusgeben(1, "Euro");
            }
            else if(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                muenzeAusgeben(50, "Cent");
            }
            else if(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                muenzeAusgeben(0.2, "CENT");
            }
            else if(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                muenzeAusgeben(0.1, "CENT");
            }
            else if(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                rückgabebetrag -= 0.05;
            }
            rückgabebetrag = Math.round(rückgabebetrag * 100.0) / 100.0;
        }
    }

    static double zuZahlenderBetrag = 0.0;
    static double eingezahlterGesamtbetrag = 0.0;
    static double eingeworfeneMünze = 0.0;
    static double rückgabebetrag = 0.0;
    static int fahrkartenAnzahl = 0;

    public static void main(String[] args) {
        Scanner tastatur = new Scanner(System.in);

        fahrkartenbestellungErfassen(tastatur);
        fahrkartenBezahlen(tastatur);
        fahrkartenAusgeben();
        rueckgeldAusgabe();


       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
}
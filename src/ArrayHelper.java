public class ArrayHelper {

    public static String convertArrayToString(int[] zahlen){
        String returner = "";
        for(int i : zahlen) {
            returner = returner + i + ",";
        }
        return returner;
    }

}